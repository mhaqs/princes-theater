# Princes Theater

Lexicon Digital Challenge

An application that displays the content from an API. The Price's theater allows you to check which movies are
available and how you can get the best price out of them.

## Resources
The application does the following:

- It creates an Api module using Retrofit that talks to the Lexicon API.
- Any data received is checked against errors and passed to the caller.
- It displays the returned data in a list corresponding to the number of movies.

## The Framework

The project takes some freedom in using frameworks from the sample app.

- It uses Koin as a dependency injection framework.
    - Koin is an alternative to Dagger for Kotlin injection. I've used Dagger a lot before but this was a good chance to test how Koin fares against it.
- Retrofit is used for the Api set up as was before but using `suspend` methods.
- Coroutines are used to perform heavy tasks from the ui to the network use cases.
- Use cases are defined to execute the coroutines, which eventually call the repository that uses the Retrofit api to retrieve the results.
    - This allows us to do batch operations in a single coroutines that can invoke multiple coroutines from the retrofit api.
- Data classes are tagged with Serialisation names, although gson could handle a policy naming strategy.
    - If the api does not adhere to a single naming strategy, using serialisation names is preferable.
- Lifecycle framework is used to scope activity to a presenter, so that if an activity exits between an operation, it releases the presenter as well.
    - There's multiple ways to do that without depending on the lifecycle as well ie by manually calling presenter's destruction methods.
- I used Model View Presenter instead of MVVM to speed up the development and testing process.

## Building
1. Clone the git repository
2. Run `./gradlew build` or `./gradlew assembledebug`

## What more can be done given time

There's probably a lot of things that can be improved given the scope of the api and task at hand.

- Explore what other options the api gives us and integrate those to make the app better.
- The movie and provider selection can be filtered.
- Auto refresh
- Animations can be used to increase user experience.
- Probably a lot more.
