package au.com.lexicon.princestheater.core.injection

import au.com.lexicon.princestheater.view.MovieDetailMVP
import au.com.lexicon.princestheater.view.MovieDetailPresenter
import au.com.lexicon.princestheater.view.MoviesMVP
import au.com.lexicon.princestheater.view.MoviesPresenter
import org.koin.dsl.module
import org.mockito.Mockito

val viewTestModule = module {
    factory<MoviesMVP.View> { Mockito.mock(MoviesMVP.View::class.java) }

    factory<MoviesMVP.Presenter> {
            MoviesPresenter(get(), get(), get())
    }

    factory<MovieDetailMVP.View> { Mockito.mock(MovieDetailMVP.View::class.java) }

    factory<MovieDetailMVP.Presenter> {
        MovieDetailPresenter(get(), get(), get())
    }
}