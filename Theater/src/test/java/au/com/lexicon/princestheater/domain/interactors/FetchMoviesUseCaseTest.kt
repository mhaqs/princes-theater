package au.com.lexicon.princestheater.domain.interactors

import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.exceptions.ServerException
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FetchMoviesUseCaseTest {
    @Mock lateinit var movieRepository: MovieRepository
    @Mock lateinit var movies: List<Movie>
    lateinit var useCase: FetchMoviesUseCase

    private var error: Exception? = null
    private var result: Any = Any()

    @Before
    fun setUp() {
        useCase = FetchMoviesUseCase(movieRepository)
        useCase.backgroundContext = Dispatchers.Unconfined
        useCase.foregroundContext = Dispatchers.Unconfined
    }

    @Test
    fun `should receive the results from repository`() {
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovies(anyString()) } doReturn UseCaseResult.Success(movies)
        }

        useCase.execute(
            MoviesDataParams("cinemaworld"),
            onSuccess = {
                result = it
                assertEquals(movies, result)
            })
    }

    @Test
    fun `should receive error from repository`() {
        val notFound = ServerException()
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovies(anyString()) } doReturn UseCaseResult.Error(notFound)
        }

        useCase.execute(
            MoviesDataParams("cinemaworld"),
            onError = {
                error = it
                assertEquals(notFound, error)
            })
    }

    @Test
    fun `should receive cancel error from repository`() {
        val notFound = ServerException()
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovies(anyString()) } doReturn UseCaseResult.Cancel(notFound)
        }

        useCase.execute(
            MoviesDataParams("cinemaworld"),
            onCancel = {
                error = it
                assertEquals(notFound, error)
            })
    }
}
