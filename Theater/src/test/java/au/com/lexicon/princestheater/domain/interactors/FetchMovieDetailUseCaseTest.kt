package au.com.lexicon.princestheater.domain.interactors

import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.domain.exceptions.ServerException
import au.com.lexicon.princestheater.domain.models.MovieDetail
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FetchMovieDetailUseCaseTest {
    @Mock lateinit var movieRepository: MovieRepository
    @Mock lateinit var movieDetail: MovieDetail
    lateinit var useCase: FetchMovieDetailUseCase

    private var error: Exception? = null
    private var result: Any = Any()

    @Before
    fun setUp() {
        useCase = FetchMovieDetailUseCase(movieRepository)
        useCase.backgroundContext = Dispatchers.Unconfined
        useCase.foregroundContext = Dispatchers.Unconfined
    }

    @Test
    fun `should receive the results from repository`() {
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovieDetail(anyString(), anyString()) } doReturn UseCaseResult.Success(movieDetail)
        }

        useCase.execute(
            MovieDetailParams("cinemaworld", "foobar"),
            onSuccess = {
                result = it
                assertEquals(movieDetail, result)
            })
    }

    @Test
    fun `should receive error from repository`() {
        val notFound = ServerException()
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovieDetail(anyString(), anyString()) } doReturn UseCaseResult.Error(notFound)
        }

        useCase.execute(
            MovieDetailParams("cinemaworld", "foobar"),
            onError = {
                error = it
                assertEquals(notFound, error)
            })
    }

    @Test
    fun `should receive cancel error from repository`() {
        val notFound = ServerException()
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovieDetail(anyString(), anyString()) } doReturn UseCaseResult.Cancel(notFound)
        }

        useCase.execute(
            MovieDetailParams("cinemaworld", "foobar"),
            onCancel = {
                error = it
                assertEquals(notFound, error)
            })
    }
}
