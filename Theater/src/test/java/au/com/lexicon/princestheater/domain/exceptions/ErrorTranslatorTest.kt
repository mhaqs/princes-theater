package au.com.lexicon.princestheater.domain.exceptions

import android.content.Context
import au.com.lexicon.princestheater.domain.exceptions.ErrorTranslator
import au.com.lexicon.princestheater.domain.exceptions.ServerException
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class ErrorTranslatorTest {
    @Mock
    private lateinit var context: Context

    @Test
    fun `should return the correct string for exception`() {
        val errorTranslator = ErrorTranslator(context)
        whenever(context.getString(anyInt())).thenReturn("No Movies Found!")
        val result = errorTranslator.translateException(ServerException())
        assertEquals("No Movies Found!", result)
    }

    @Test
    fun `should return the default string if no exception is matched`() {
        val errorTranslator = ErrorTranslator(context)
        whenever(context.getString(anyInt())).thenReturn("Oops! something went wrong")
        val result = errorTranslator.translateException(IllegalArgumentException())
        assertEquals("Oops! something went wrong", result)
    }
}