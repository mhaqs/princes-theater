package au.com.lexicon.princestheater.domain.models

import au.com.lexicon.princestheater.data.models.Movie
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class MovieDetailTest {

    @Test
    fun `should set movie detail by provider`() {
        val movieDetail = MovieDetail("123")
        movieDetail.setMovieByProvider("cinemaworld", Movie("cw123"))
        assertEquals("cw123", movieDetail.getMovieByProvider("cinemaworld")?.id)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `should throw exception if movie detail ID does not match used detail`() {
        val movieDetail = MovieDetail("123")
        movieDetail.setMovieByProvider("cinemaworld", Movie("cw12"))
    }

    @Test
    fun `should get movie detail price by provider`() {
        val movieDetail = MovieDetail("123")
        movieDetail.setMovieByProvider("cinemaworld", Movie("cw123", price = 2.37))
        assertEquals("cw123", movieDetail.getMovieByProvider("cinemaworld")?.id)
        assertEquals(2.37, movieDetail.getMovieByProvider("cinemaworld")?.price)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `should throw exception if id is non numeric`() {
        MovieDetail("fw123")
    }
}