package au.com.lexicon.princestheater.domain.repository

import au.com.lexicon.princestheater.data.api.SiteApi
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.data.models.MoviesResponse
import au.com.lexicon.princestheater.domain.exceptions.ServerException
import au.com.lexicon.princestheater.domain.extensions.nakedId
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class MovieRepositoryTest {

    private lateinit var movieRepository: MovieRepository

    @Mock
    private lateinit var siteApi: SiteApi

    private val movie = Movie(
        "fw2488496",
        "Star Wars: Episode VII - The Force Awakens",
        type="movie",
        poster = "https://m.media-amazon.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX300.jpg",
        price = 2.37
    )
    private val movie2 = Movie(
        "fw2488497",
        "Star Wars: Episode VIII - The Force Goes to Sleep",
        type="movie",
        poster = "https://m.media-amazon.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX300.jpg",
        price = 8.88
    )
    private val moviesResponse = MoviesResponse("filmworld", listOf(movie, movie2))

    @Before
    fun setUp() {
        movieRepository = MovieRepositoryImpl(siteApi)
    }

    @Test
    fun `should return cancel when the provider is empty`() {
        runBlocking {
            val res = movieRepository.getMovies("")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is CancellationException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return cancel when the provider is empty for detail`() {
        runBlocking {
            val res = movieRepository.getMovieDetail("","foobar")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is CancellationException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return illegal exception when the movie id is naked for detail`() {
        runBlocking {
            val res = movieRepository.getProviderPricingForMovie(listOf("",""),"1234")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is IllegalArgumentException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return cancel when the movie id is empty for detail`() {
        runBlocking {
            val res = movieRepository.getMovieDetail("cinemaworld","")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is CancellationException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return cancel when the providers list is empty for pricing`() {
        runBlocking {
            val res = movieRepository.getProviderPricingForMovie(emptyList(),"foobar")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is CancellationException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return cancel when the movie id is empty for pricing`() {
        runBlocking {
            val res = movieRepository.getProviderPricingForMovie(listOf("",""),"")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is CancellationException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return illegal exception when the movie id is not naked for pricing`() {
        runBlocking {
            val res = movieRepository.getProviderPricingForMovie(listOf("",""),"fw1234")
            assertTrue(res.isFailure)
            res.map(
                errored = { },
                cancelled = { assertTrue(it is IllegalArgumentException) },
                succeeded = {}
            )
        }
    }

    @Test
    fun `should return error when the movie data is empty or no movies were found`() {
        mock<SiteApi>() {
            onBlocking {
                siteApi.fetchMovies(anyString())
            } doReturn MoviesResponse("filmworld", emptyList())
        }
        runBlocking {
            val res = movieRepository.getMovies("filmworld")
            assertTrue(res.isFailure)
            res.map(
                succeeded = {},
                cancelled = {},
                errored = { assertTrue(it is ServerException) }
            )
        }
    }

    @Test
    fun `should return movie data when data is retrieved`() {
        mock<SiteApi> {
            onBlocking {
                siteApi.fetchMovies(anyString())
            } doReturn moviesResponse
        }
        runBlocking {
            val res = movieRepository.getMovies("filmworld")
            assertTrue(res.isSuccessful)
            res.map(
                succeeded = {
                    assertTrue(it.size == 2)
                },
                cancelled = {},
                errored = {}
            )
        }
    }

    @Test
    fun `should return movie detail when data is retrieved`() {
        mock<SiteApi> {
            onBlocking {
                siteApi.fetchMovie(anyString(), anyString())
            } doReturn movie
        }
        runBlocking {
            val res = movieRepository.getMovieDetail("filmworld", "foobar")
            assertTrue(res.isSuccessful)
            res.map(
                succeeded = {
                    assertEquals(it.ID, movie.nakedId)
                    assertEquals(it.getMovieByProvider("filmworld"), movie)
                },
                cancelled = {},
                errored = {}
            )
        }
    }

    @Test
    fun `should return movie pricing when data is retrieved`() {
        mock<SiteApi> {
            onBlocking {
                siteApi.fetchMovie(anyString(), anyString())
            } doReturn movie
        }
        runBlocking {
            val res = movieRepository.getProviderPricingForMovie(listOf("filmworld"), "2488496")
            assertTrue(res.isSuccessful)
            res.map(
                succeeded = {
                    assertEquals(it.ID, movie.nakedId)
                    assertEquals(it.getPriceForMovieByProvider("filmworld"), 2.37)
                },
                cancelled = {},
                errored = {}
            )
        }
    }
}