package au.com.lexicon.princestheater

import android.app.Application
import au.com.lexicon.princestheater.core.injection.appModule
import au.com.lexicon.princestheater.core.injection.dataModule
import au.com.lexicon.princestheater.core.injection.viewTestModule
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.koinApplication
import org.koin.test.KoinTest
import org.koin.test.check.checkModules
import org.mockito.Mockito.mock

class ModuleCheckTest : KoinTest {

    private val mockedAndroidContext = mock(Application::class.java)

    @Test
    fun testAppConfiguration() {
        koinApplication {
            fileProperties()
            androidContext(mockedAndroidContext)
            modules(viewTestModule + dataModule + appModule)
        }.checkModules()
    }
}