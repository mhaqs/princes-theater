package au.com.lexicon.princestheater.core.usecase

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class UseCaseTest {
    private var startCalled = false
    private var completeCalled = false
    private var result: Any? = Any()
    private var canceledCalled = false
    private var error: Exception? = null

    @Before
    fun setUp() {
        startCalled = false
        completeCalled = false
        result = Any()
        canceledCalled = false
        error = null
    }

    private fun setUpUseCase(useCase: UseCase<String, UseCase.EmptyParams>) = useCase.apply {
        foregroundContext = Dispatchers.Unconfined
        backgroundContext = Dispatchers.Unconfined
        execute(
            UseCase.EmptyParams(),
            onStart = { startCalled = true },
            onFinish = { completeCalled = true },
            onSuccess = { result = it },
            onCancel = { canceledCalled = true },
            onError = { error = it })
    }

    @Test
    fun shouldCallOnErrorOnFailure() = runBlockingTest {
        val exception = RuntimeException("i am le tired")
        setUpUseCase(object : UseCase<String, UseCase.EmptyParams>() {
            override suspend fun executeOnBackground(params: UseCase.EmptyParams): UseCaseResult<Exception, String> {
                throw exception
            }
        })
        assertEquals(exception.message, error?.message)
    }

    @Test
    fun shouldCallSuccessOnSuccess() = runBlockingTest {
        val res = UseCaseResult.Success("then fire ze missiles")
        setUpUseCase(object : UseCase<String, UseCase.EmptyParams>() {
            override suspend fun executeOnBackground(params: EmptyParams) = res
        })
        assertEquals(error, null)
        assertEquals(result, res.success)
    }

    @Test
    fun shouldThrowErrorOnSuccessCallFailure() = runBlockingTest {
        val result = UseCaseResult.Success("then fire ze missiles")
        var errorCalled = false
        var completeCalled = false
        var successCalled = false
        var exceptionCaught = false
        val useCase = object : UseCase<String, UseCase.EmptyParams>() {
            override suspend fun executeOnBackground(params: EmptyParams): UseCaseResult<Exception, String> =
                result
        }
        useCase.backgroundContext = Dispatchers.Unconfined
        useCase.foregroundContext = Dispatchers.Unconfined
        useCase.context = CoroutineExceptionHandler { _, _ ->
            exceptionCaught = true
        }
        useCase.execute(
            UseCase.EmptyParams(),
            onSuccess = {
                successCalled = true
                assertEquals(it, result)
                throw RuntimeException("Well I never")
            },
            onError = {
                errorCalled = true
            },
            onFinish = {
                completeCalled = true
            })
        assertEquals(exceptionCaught, true)
        assertEquals(errorCalled, false)
        assertEquals(successCalled, true)
        assertEquals(completeCalled, true)
    }

    @Test
    fun shouldSupportNullResultsWithoutTriggeringError() {
        val useCase = object : UseCase<String?, UseCase.EmptyParams>() {
            override suspend fun executeOnBackground(params: EmptyParams): UseCaseResult<Exception, String?> {
                return UseCaseResult.Success(null)
            }
        }
        useCase.foregroundContext = Dispatchers.Unconfined
        useCase.backgroundContext = Dispatchers.Unconfined
        useCase.execute(
            UseCase.EmptyParams(),
            onStart = {
                println("start")
                startCalled = true
            },
            onFinish = {
                println("finish")
                completeCalled = true
            },
            onSuccess = {
                println("success")
                result = it
            },
            onCancel = {
                println("cancel")
                canceledCalled = true
            },
            onError = {
                println("error")
                error = it
            })

        assertEquals(canceledCalled, false)
        assertEquals(result, null)
        assertEquals(completeCalled, true)
    }
}
