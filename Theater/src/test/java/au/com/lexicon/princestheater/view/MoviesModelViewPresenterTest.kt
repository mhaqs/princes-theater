package au.com.lexicon.princestheater.view

import android.content.Context
import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.domain.exceptions.ErrorTranslator
import au.com.lexicon.princestheater.domain.exceptions.ServerException
import au.com.lexicon.princestheater.domain.interactors.FetchMoviesUseCase
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MoviesModelViewPresenterTest {

    @Mock lateinit var mvpView: MoviesMVP.View
    @Mock lateinit var movieRepository: MovieRepository
    @Mock lateinit var errorTranslator: ErrorTranslator
    @Mock lateinit var context: Context
    private lateinit var fetchMoviesUseCase: FetchMoviesUseCase
    private lateinit var moviesPresenter: MoviesMVP.Presenter

    @Before
    fun setUp() {
        fetchMoviesUseCase = FetchMoviesUseCase(movieRepository)
        fetchMoviesUseCase.backgroundContext = Dispatchers.Unconfined
        fetchMoviesUseCase.foregroundContext = Dispatchers.Unconfined
    }

    @Test
    fun `should return the result to the view`() {
        moviesPresenter =
            MoviesPresenter(mvpView, errorTranslator, fetchMoviesUseCase)
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovies(anyString()) } doReturn UseCaseResult.Success(
                emptyList())
        }
        moviesPresenter.loadMovies("cinemaworld")
        verify(mvpView).showMovies(emptyList())
    }

    @Test
    fun `should ask the view to display an error when movies to fetch was canceled`() {
        val noMoviesFoundException = ServerException()
        moviesPresenter =
            MoviesPresenter(mvpView, errorTranslator, fetchMoviesUseCase)
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovies(anyString()) } doReturn UseCaseResult.Cancel(
            noMoviesFoundException)
        }
        moviesPresenter.loadMovies("filmworld")
        verify(mvpView).showError(anyOrNull())
    }

    @Test
    fun `should ask the view to display the correct error translated`() {
        val noMoviesFoundException = ServerException()
        whenever(context.getString(anyInt())).thenReturn("No Movies Found!")
        val errorTranslator = ErrorTranslator(context)
        moviesPresenter =
            MoviesPresenter(mvpView, errorTranslator, fetchMoviesUseCase)
        mock<MovieRepository>() {
            onBlocking { movieRepository.getMovies(anyString()) } doReturn UseCaseResult.Cancel(
                noMoviesFoundException)
        }
        moviesPresenter.loadMovies("filmworld")
        verify(mvpView).showError("No Movies Found!")
    }
}