package au.com.lexicon.princestheater.domain.exceptions

import android.content.Context
import au.com.lexicon.princestheater.R
import kotlinx.coroutines.CancellationException
import okio.IOException
import retrofit2.HttpException

/**
 * The error translator is a utility class that helps with translating user facing
 * exceptions and errors.
 *
 * The class does not log the exceptions to the console. It only returns the
 * appropriate translation for the received error.
 *
 * The multilingual translation of errors should go into the strings.xml
 *
 * @param context The context injected or provided by the caller
 */
class ErrorTranslator(private val context: Context) {

    /**
     * The method maps the given exception to a user facing string
     * translated to the language that the application is running in.
     *
     * @param exception The exception that was sent by the caller
     *
     * @return String The translated/mapped error string for that exception
     */
    fun translateException(exception: Exception): String {
        return when (exception) {
            is CancellationException -> context.getString(R.string.cancellation_error)
            is IOException -> context.getString(R.string.network_error)
            is HttpException -> context.getString(R.string.network_error)
            else -> context.getString(R.string.oops_something_went_wrong_error)
        }
    }
}