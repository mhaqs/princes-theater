package au.com.lexicon.princestheater.domain.interactors

import au.com.lexicon.princestheater.core.usecase.UseCase
import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.models.MovieDetail
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import org.jetbrains.annotations.NotNull

/**
 * The use case to fetch movie detail from the api.
 *
 * @param movieRepository The repository used to query the api
 */
class FetchMovieDetailUseCase(
    private val movieRepository: MovieRepository
) : UseCase<MovieDetail, MovieDetailParams>() {
    override suspend fun executeOnBackground(params: MovieDetailParams): UseCaseResult<Exception, MovieDetail> =
        movieRepository.getMovieDetail(params.provider, params.movieId)
}

data class MovieDetailParams(@NotNull val provider: String, @NotNull val movieId: String)