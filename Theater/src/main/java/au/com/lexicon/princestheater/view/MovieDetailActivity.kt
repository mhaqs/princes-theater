package au.com.lexicon.princestheater.view

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import au.com.lexicon.princestheater.R
import au.com.lexicon.princestheater.core.injection.extensions.lifecycleScope
import au.com.lexicon.princestheater.databinding.MovieDetailActivityBinding
import au.com.lexicon.princestheater.domain.models.MovieDetail
import au.com.lexicon.princestheater.view.adapter.MOVIE_ID
import au.com.lexicon.princestheater.view.adapter.TRANSITION_NAME
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import org.koin.core.parameter.parametersOf
import java.util.*

class MovieDetailActivity : AppCompatActivity(), MovieDetailMVP.View {
    // The lifecycle framework allows us to bind the presenter to the activity's lifecycle
    private val moviesPresenter: MovieDetailMVP.Presenter by lifecycleScope.inject {
        parametersOf(
            this@MovieDetailActivity
        )
    }

    private lateinit var binding: MovieDetailActivityBinding

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.movie_detail_activity)

        // Use the movie id and transition name to load
        // pricing details
        if (intent.hasExtra(MOVIE_ID)) {
            val movieId = intent.getStringExtra(MOVIE_ID)
            val transitionName = intent.getStringExtra(TRANSITION_NAME)
            binding.poster.transitionName = transitionName
            updateMovieDetail(movieId)

            binding.helperButton.setOnClickListener {
                updateMovieDetail(movieId)
            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun updateMovieDetail(movieId: String) {
        // load pricing detail from all providers
        moviesPresenter.loadMovieDetail(
            listOf(
                getString(R.string.cinemaworld).toLowerCase(Locale.getDefault()),
                getString(R.string.filmworld).toLowerCase(Locale.getDefault())
            ), movieId
        )
    }

    override fun showMovieDetail(movieDetail: MovieDetail) {
        hideLoading()

        // get movie detail after successful callback
        val movie =
            movieDetail.getMovieByProvider(getString(R.string.filmworld).toLowerCase(Locale.getDefault()))
        binding.title.text = movie?.title

        // get individual price
        val filmWorldPrice =
            movieDetail.getPriceForMovieByProvider(getString(R.string.filmworld).toLowerCase(Locale.getDefault()))
        val cinemaWorldPrice =
            movieDetail.getPriceForMovieByProvider(getString(R.string.cinemaworld).toLowerCase(Locale.getDefault()))

        // show the price
        binding.filmworldPrice.text =
            String.format(getString(R.string.filmworld_price), filmWorldPrice)
        binding.cinemaworldPrice.text =
            String.format(getString(R.string.cinemaworld_price), cinemaWorldPrice)

        // make a recommendation
        binding.recommendedPrice.text = if (filmWorldPrice < cinemaWorldPrice) {
            String.format(getString(R.string.recommended_price), getString(R.string.filmworld))
        } else {
            String.format(getString(R.string.recommended_price), getString(R.string.cinemaworld))
        }

        // Load the poster
        Glide.with(this)
            .load(movie?.poster)
            .transition(withCrossFade())
            .into(binding.poster)
    }

    override fun showError(error: String) {
        binding.apply {
            dataVisible = true
            helperText.text = error
            helperText.visibility = View.VISIBLE
            helperButton.visibility = View.VISIBLE
        }
    }

    override fun showLoading() {
        binding.apply {
            dataVisible = true
            binding.helperText.text = getString(R.string.loading)
            binding.helperText.visibility = View.VISIBLE
        }
    }

    override fun hideLoading() {
        binding.apply {
            dataVisible = false
            helperText.visibility = View.GONE
            helperButton.visibility = View.GONE
        }
    }
}
