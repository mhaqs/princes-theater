package au.com.lexicon.princestheater.core.injection

import android.content.Context
import au.com.lexicon.princestheater.view.*
import au.com.lexicon.princestheater.view.adapter.MoviesAdapter
import org.koin.dsl.module

// The activity module defines instances of view related injections
val activityModule = module {
    // In MVP, mostly an activity is used as a coordinator for the presenter
    // The lifecycle of the presenter and any operations that it performs should be
    // tied to that of the activity. The scope operator allows us to create a
    // scoped instance of the presenter for the injecting activity.
    scope<MoviesActivity> {
        scoped<MoviesMVP.Presenter> { (view: MoviesMVP.View) ->
            MoviesPresenter(view, get(), get())
        }
    }

    scope<MovieDetailActivity> {
        scoped<MovieDetailMVP.Presenter> { (view: MovieDetailMVP.View) ->
            MovieDetailPresenter(view, get(), get())
        }
    }

    // Creates an instance of the adapter whenever called
    factory { (context: Context) -> MoviesAdapter(context) }
}