package au.com.lexicon.princestheater.view

import au.com.lexicon.princestheater.domain.models.MovieDetail

/**
 * A Movie Detail Model View Presenter Pattern. The View and Presenters
 * must conform to this contract to make sure that the injection
 * works as expected.
 */
interface MovieDetailMVP {

    interface View {
        /**
         * Load the movie detail from the server
         */
        fun updateMovieDetail(movieId: String)

        /**
         * Show the received movie detail from the server
         *
         * @param movieDetail the detail received from the server
         */
        fun showMovieDetail(movieDetail: MovieDetail)

        /**
         * Shows any error received from the presenter
         *
         * @param error The error to display
         */
        fun showError(error: String)

        /**
         * Show a loading indicator
         */
        fun showLoading()

        /**
         * Hide the loading indicator
         */
        fun hideLoading()
    }

    interface Presenter {
        /**
         * Responsible for calling and receiving the server response
         * for the published movie
         */
        fun loadMovieDetail(providers: List<String>, movieId: String)

        /**
         * Process any errors received from the server as a user
         * facing string, or log it as required
         *
         * @param it the exception
         */
        fun processError(it: Exception)
    }
}