package au.com.lexicon.princestheater.core.injection

import au.com.lexicon.princestheater.domain.exceptions.ErrorTranslator
import au.com.lexicon.princestheater.domain.interactors.FetchMovieDetailUseCase
import au.com.lexicon.princestheater.domain.interactors.FetchMoviesUseCase
import au.com.lexicon.princestheater.domain.interactors.FetchProviderPricingUseCase
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import au.com.lexicon.princestheater.domain.repository.MovieRepositoryImpl
import org.koin.dsl.module

// The app module brings together other modules and defines
// any global instances used across the app.
val appModule = module {
    // creates an instance of the use cases whenever injected
    factory { FetchMoviesUseCase(get()) }

    factory { FetchMovieDetailUseCase(get()) }

    factory { FetchProviderPricingUseCase(get()) }

    single { ErrorTranslator(get()) }

    // Creates a single instance of the repository at the start of the module creation
    single<MovieRepository>(createdAtStart = true) {
        MovieRepositoryImpl(get())
    }
}

// Static class to map the variables in koin.properties
object Properties {
    const val SERVER_URL = "SERVER_URL"
    const val X_API_KEY = "X-API-KEY"
    const val CONNECT_TIMEOUT = "CONNECT_TIMEOUT"
    const val READ_TIMEOUT = "READ_TIMEOUT"
}

val theaterApp = listOf(appModule, activityModule, dataModule)