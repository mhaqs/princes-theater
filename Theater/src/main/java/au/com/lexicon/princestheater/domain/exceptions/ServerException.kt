package au.com.lexicon.princestheater.domain.exceptions

/**
 * A custom exception thrown when something goes wrong on the server.
 *
 * The default message provided is for ease of development only.
 * The error message should be translated using #ErrorTranslator class
 * and provided to the View for user consumption, as required.
 *
 * @param message The default message to show on exception
 */
class ServerException
    (message: String = "Internal server error") : Exception(message)