package au.com.lexicon.princestheater.domain.models

import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.extensions.movieIdIsNaked
import au.com.lexicon.princestheater.domain.extensions.nakedId

/**
 * Movie Detail is a domain model ie a representation of the domain entity
 * that we're trying to show to the user.
 *
 * It is comprised of information from a #Movie and price. The
 * information contained within the domain object is not the same as the
 * data models. The data models keep the original information received from
 * the server.
 */
data class MovieDetail(
    val ID: String
) {
    init {
        if (!movieIdIsNaked(ID)) throw IllegalArgumentException("ID must be numeric, was $ID")
    }

    private val providerMap: HashMap<String, Movie> = HashMap()

    fun getPriceForMovieByProvider(provider: String): Double = providerMap[provider]?.price ?: 0.0

    fun setMovieByProvider(provider: String, movie: Movie) {
        if (movie.nakedId != ID) throw IllegalArgumentException("$ID does not match ${movie.nakedId}")

        providerMap.put(provider, movie)
    }

    fun getMovieByProvider(provider: String): Movie? = providerMap[provider]
}