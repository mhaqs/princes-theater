package au.com.lexicon.princestheater.view

import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.models.MovieDetail

/**
 * A Movies Model View Presenter Pattern. The View and Presenters
 * must conform to this contract to make sure that the injection
 * works as expected.
 */
interface MoviesMVP {

    interface View {
        /**
         * Load the movies from the server
         */
        fun updateMoviesCatalogue()

        /**
         * Show the received movies from the server
         *
         * @param movies the list of movies received from the server
         */
        fun showMovies(movies: List<Movie>)

        /**
         * Shows any error received from the presenter
         *
         * @param error The error to display
         */
        fun showError(error: String)

        /**
         * Show a loading indicator
         */
        fun showLoading()

        /**
         * Hide the loading indicator
         */
        fun hideLoading()
    }

    interface Presenter {
        /**
         * Responsible for calling and receiving the server response
         * for the published movies
         */
        fun loadMovies(provider: String)

        /**
         * Process any errors received from the server as a user
         * facing string, or log it as required
         *
         * @param it the exception
         */
        fun processError(it: Exception)
    }
}