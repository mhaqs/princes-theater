package au.com.lexicon.princestheater.domain.interactors

import au.com.lexicon.princestheater.core.usecase.UseCase
import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.models.MovieDetail
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import org.jetbrains.annotations.NotNull

/**
 * The use case to fetch movies from the api.
 * Its only purpose is to fetch and return the movies
 * using the api endpoint.
 *
 * @param movieRepository The repository used to query the api
 */
class FetchMoviesUseCase(
    private val movieRepository: MovieRepository
) : UseCase<List<Movie>, MoviesDataParams>() {
    override suspend fun executeOnBackground(params: MoviesDataParams): UseCaseResult<Exception, List<Movie>> =
        movieRepository.getMovies(params.provider)
}

data class MoviesDataParams(@NotNull val provider: String)