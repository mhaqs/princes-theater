package au.com.lexicon.princestheater.data.models

import com.google.gson.annotations.SerializedName

data class MoviesResponse(
    @SerializedName("Provider")
    val provider: String,
    @SerializedName("Movies")
    val movies: List<Movie>
)
