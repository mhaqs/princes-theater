package au.com.lexicon.princestheater.data.models

import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("ID")
    val id: String = "",
    @SerializedName("Title")
    val title: String = "",
    @SerializedName("Year")
    val year: Int = 0,
    @SerializedName("Rated")
    val rated: String = "",
    @SerializedName("Released")
    val released: String = "",
    @SerializedName("Runtime")
    val runtime: String = "",
    @SerializedName("Genre")
    val genre: String = "",
    @SerializedName("Director")
    val director: String = "",
    @SerializedName("Writer")
    val writer: String = "",
    @SerializedName("Actors")
    val actors: String = "",
    @SerializedName("Plot")
    val plot: String = "",
    @SerializedName("Language")
    val language: String = "",
    @SerializedName("Country")
    val country: String = "",
    @SerializedName("Poster")
    val poster: String = "",
    @SerializedName("Type")
    val type: String = "",
    @SerializedName("Production")
    val production: String = "",
    @SerializedName("Price")
    val price: Double = 0.0
)

