package au.com.lexicon.princestheater.core.usecase

/**
 * A class used to fold the results returned from a use case. There are normally only
 * two states of a use case ie Success or Failure.
 *
 * However, some use cases can also have other results like Cancellation, if
 *
 * - program exited
 * - coroutine failed
 * - network cancellation
 * - api returned something other than an error
 *
 * Cancellation is still a failure state, and that's what the map function does.
 * It will map the given result into value required.
 *
 * - Success will have the value S that we're looking for
 * - Failure will have the exception E
 */
sealed class UseCaseResult<out E, out S> {

    val isSuccessful get() = this is Success<S>
    val isFailure get() = this is Error<E> || this is Cancel<E>

    inline fun <T> map(cancelled: (E) -> T, errored: (E) -> T, succeeded: (S) -> T): T =
        when (this) {
            is Cancel -> cancelled(cancel)
            is Error -> errored(error)
            is Success -> succeeded(success)
        }

    data class Cancel<out E>(val cancel: E) : UseCaseResult<E, Nothing>()
    data class Error<out E>(val error: E) : UseCaseResult<E, Nothing>()
    data class Success<out S>(val success: S) : UseCaseResult<Nothing, S>()
}