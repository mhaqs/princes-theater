package au.com.lexicon.princestheater.core.injection

import au.com.lexicon.princestheater.core.injection.Properties.CONNECT_TIMEOUT
import au.com.lexicon.princestheater.core.injection.Properties.READ_TIMEOUT
import au.com.lexicon.princestheater.core.injection.Properties.SERVER_URL
import au.com.lexicon.princestheater.core.injection.Properties.X_API_KEY
import au.com.lexicon.princestheater.data.api.SiteApi
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val API_KEY_HEADER = "x-api-key"

// The data module contains source and repository injections
val dataModule = module {
    // creates a single instance of the okhttp client
    // the properties are retrieved from koin.properties using
    // Koin's `getProperty` method
    single {
        createOkHttpClient(
            getProperty(CONNECT_TIMEOUT),
            getProperty(READ_TIMEOUT),
            getProperty(X_API_KEY)
        )
    }

    // creates a single instance of the Site Api for Retrofit
    single { createWebService<SiteApi>(get(), getProperty(SERVER_URL)) }
}

/**
 * Utility method to create an okhttp client
 *
 * @param connectTimeOutInSeconds the timeout for connecting to the server
 * @param readTimeoutInSeconds the timeout for reading a response
 *
 * @return the okhttp client
 */
fun createOkHttpClient(
    connectTimeOutInSeconds: Long,
    readTimeoutInSeconds: Long,
    apiHeaderKey: String
): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(connectTimeOutInSeconds, TimeUnit.SECONDS)
        .readTimeout(readTimeoutInSeconds, TimeUnit.SECONDS)
        .addInterceptor {
            val request = it
                .request()
                .newBuilder()
                .addHeader(API_KEY_HEADER, apiHeaderKey)
                .build()
            it.proceed(request)
        }
        .addInterceptor(httpLoggingInterceptor).build()
}

/**
 * Utility method to create a web service for Retrofit interface
 *
 * @param okHttpClient the okhttp client to use for requests
 * @param url the base url of the server
 *
 * @return T the service this instance is created for
 */
inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())).build()
    return retrofit.create(T::class.java)
}