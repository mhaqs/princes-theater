package au.com.lexicon.princestheater.view

import au.com.lexicon.princestheater.domain.exceptions.ErrorTranslator
import au.com.lexicon.princestheater.domain.interactors.FetchMoviesUseCase
import au.com.lexicon.princestheater.domain.interactors.MoviesDataParams
import timber.log.Timber

class MoviesPresenter(
    private val view: MoviesMVP.View,
    private val errorTranslator: ErrorTranslator,
    private val fetchMoviesUseCase: FetchMoviesUseCase
) : MoviesMVP.Presenter {
    override fun loadMovies(provider: String) {
        fetchMoviesUseCase.execute(
            MoviesDataParams(provider),
            onStart = { view.showLoading() },
            onFinish = { view.hideLoading() },
            onCancel = { processError(it) },
            onError = { processError(it) },
            onSuccess = { view.showMovies(it) }
        )
    }

    override fun processError(it: Exception) {
        Timber.e(it)
        // Log the error, but translate it so that the user
        // can see it in their language.
        view.showError(errorTranslator.translateException(it))
    }
}