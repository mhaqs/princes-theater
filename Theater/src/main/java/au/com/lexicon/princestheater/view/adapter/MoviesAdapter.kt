package au.com.lexicon.princestheater.view.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import au.com.lexicon.princestheater.R
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.extensions.nakedId
import au.com.lexicon.princestheater.view.MovieDetailActivity
import com.bumptech.glide.Glide
import org.jetbrains.annotations.Nullable
import java.util.*


const val MOVIE_ID = "MOVIE_ID"
const val TRANSITION_NAME = "TRANSITION_NAME"

/**
 * The adapter is a mediator between the view and the data source.
 * Whenever new data is retrieved, it translates and loads those changes for the
 * view.
 */
class MoviesAdapter(
    private val context: Context,
    private var list: ArrayList<Movie> = arrayListOf()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun updateMovies(movies: List<Movie>) {
        // if there is no previous data, simply add all the data
        // and notify the adapter that the data set has changed
        if (list.isEmpty()) {
            list.addAll(movies)
            notifyDataSetChanged()
        } else {
            // If there is existing data, find out the
            // differences, then dispatch the updates to those
            // results.
            val diffCallback = MoviesDiffCallback(this.list, movies)
            val result = DiffUtil.calculateDiff(diffCallback)

            this.list.clear()
            this.list.addAll(movies);
            result.dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        // based on the view type, inflate the correct layout
        return MovieViewHolder(
            layoutInflater.inflate(
                R.layout.movie_list_fragment_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // binds the views to the holder
        when (holder) {
            is MovieViewHolder -> holder.bindView(list[holder.adapterPosition])
        }
    }

    override fun getItemCount(): Int = list.size

    /**
     * Clears the adapter of all data
     */
    fun clear() {
        this.list.clear()
        notifyDataSetChanged()
    }

    /**
     * A View holder pattern that binds the movie domain model to the
     * ViewModel ie MovieViewHolder
     *
     * @param view The View bound to this holder
     */
    open inner class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var movie: Movie? = null

        open fun bindView(movieModel: Movie) {
            movie = movieModel

            val poster = itemView.findViewById<ImageView>(R.id.poster)
            val title = itemView.findViewById<TextView>(R.id.title)

            title.text = movieModel.title

            // Load the featured image, if it's not empty
            if (movieModel.poster.isNotEmpty()) {
                Glide.with(context)
                    .load(movieModel.poster)
                    .into(poster)
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                poster.transitionName = "transition$layoutPosition";
            }

            itemView.setOnClickListener {
                if (movie != null) {
                    val id = movie?.nakedId
                    val intent = Intent(context, MovieDetailActivity::class.java).apply {
                        putExtra(MOVIE_ID, id)
                        putExtra(TRANSITION_NAME, poster.transitionName)
                    }
                    context.startActivity(intent)
                }
            }
        }
    }

    /**
     * The DiffCallback allows for smoother transition when a
     * bigger change of objects is required. It only dispatches
     * updates to the adapter for the required changes only.
     */
    class MoviesDiffCallback(
        private val oldMovies: List<Movie>,
        private val newMovies: List<Movie>
    ) :
        DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldMovies.size
        }

        override fun getNewListSize(): Int {
            return newMovies.size
        }

        override fun areItemsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Boolean {
            return oldMovies[oldItemPosition].id == newMovies[newItemPosition].id
        }

        override fun areContentsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Boolean {
            val oldMovie: Movie = oldMovies[oldItemPosition]
            val newMovie: Movie = newMovies[newItemPosition]
            return oldMovie == newMovie
        }

        @Nullable
        override fun getChangePayload(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Any? {
            return super.getChangePayload(oldItemPosition, newItemPosition)
        }
    }
}