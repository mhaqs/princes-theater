package au.com.lexicon.princestheater.domain.interactors

import au.com.lexicon.princestheater.core.usecase.UseCase
import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.models.MovieDetail
import au.com.lexicon.princestheater.domain.repository.MovieRepository
import org.jetbrains.annotations.NotNull

/**
 * The use case to fetch pricing for all providers from the api.
 *
 * @param movieRepository The repository used to query the api
 */
class FetchProviderPricingUseCase(
    private val movieRepository: MovieRepository
) : UseCase<MovieDetail, MoviePricingParams>() {
    override suspend fun executeOnBackground(params: MoviePricingParams): UseCaseResult<Exception, MovieDetail> =
        movieRepository.getProviderPricingForMovie(params.providers, params.movieId)
}

data class MoviePricingParams(@NotNull val providers: List<String>, @NotNull val movieId: String)