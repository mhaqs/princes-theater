package au.com.lexicon.princestheater.view

import au.com.lexicon.princestheater.domain.exceptions.ErrorTranslator
import au.com.lexicon.princestheater.domain.interactors.*
import timber.log.Timber

class MovieDetailPresenter(
    private val view: MovieDetailMVP.View,
    private val errorTranslator: ErrorTranslator,
    private val fetchMoviePricingUseCase: FetchProviderPricingUseCase
) : MovieDetailMVP.Presenter {
    override fun loadMovieDetail(providers: List<String>, movieId: String) {
        fetchMoviePricingUseCase.execute(
            MoviePricingParams(providers, movieId),
            onStart = { view.showLoading() },
            onFinish = { view.hideLoading() },
            onCancel = { processError(it) },
            onError = { processError(it) },
            onSuccess = { view.showMovieDetail(it) }
        )
    }

    override fun processError(it: Exception) {
        Timber.e(it)
        // Log the error, but translate it so that the user
        // can see it in their language.
        view.showError(errorTranslator.translateException(it))
    }
}