package au.com.lexicon.princestheater.data.api

import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.data.models.MoviesResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface SiteApi {
    /**
     * The REST endpoint to retrieve the detail of a specific movie title.
     *
     * https://challenge.lexicondigital.com.au/api/{cinemaworld or filmworld}/movie/{ID}
     *
     * @param provider The provider to fetch data from
     * @param movieId The id of the movie to fetch details for
     *
     * @return Movie The response json spec as defined in the endpoint
     */
    @GET("api/{provider}/movie/{movieId}")
    suspend fun fetchMovie(
        @Path("provider") provider: String,
        @Path("movieId") movieId: String
    ): Movie

    /**
     * The REST endpoint to retrieve the movies available.
     *
     * https://challenge.lexicondigital.com.au/api/{cinemaworld or filmworld}/movies
     *
     * @param provider The provider to fetch data from
     *
     * @return MoviesResponse The response json spec as defined in the endpoint
     */
    @GET("/api/{provider}/movies")
    suspend fun fetchMovies(@Path("provider") provider: String): MoviesResponse
}