package au.com.lexicon.princestheater.domain.extensions

import au.com.lexicon.princestheater.data.models.Movie

val Movie.nakedId get() = id.substring(2, id.length)

fun generateProviderIdByNakedId(provider: String, nakedId: String): String? {
    return when (provider) {
        "filmworld" -> "fw$nakedId"
        "cinemaworld" -> "cw$nakedId"
        else -> null
    }
}

fun movieIdIsNaked(movieId: String) = movieId.matches("\\d+?".toRegex())