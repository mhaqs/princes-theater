package au.com.lexicon.princestheater.view

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.lexicon.princestheater.R
import au.com.lexicon.princestheater.core.injection.extensions.lifecycleScope
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.databinding.MoviesActivityBinding
import au.com.lexicon.princestheater.view.adapter.MoviesAdapter
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.util.*


class MoviesActivity : AppCompatActivity(), MoviesMVP.View {
    // The lifecycle framework allows us to bind the presenter to the activity's lifecycle
    private val moviesPresenter: MoviesMVP.Presenter by lifecycleScope.inject {
        parametersOf(
            this@MoviesActivity
        )
    }

    // The adapter bound to the recycler views
    private val moviesAdapter: MoviesAdapter by inject { parametersOf(this@MoviesActivity) }

    private lateinit var binding: MoviesActivityBinding

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MoviesActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.helperButton.setOnClickListener {
            updateMoviesCatalogue()
        }

        prepareListView()

        // Once we've built our Activity, or we've returned
        // from the background, we refresh the movies
        updateMoviesCatalogue()
    }

    /**
     * Prepares the RecyclerView and binds it to the Adapter
     */
    private fun prepareListView() {
        binding.apply {
            moviesList.layoutManager =
                LinearLayoutManager(this@MoviesActivity, LinearLayoutManager.VERTICAL, false)
            moviesList.addItemDecoration(
                DividerItemDecoration(
                    this@MoviesActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
            moviesList.adapter = moviesAdapter
        }
    }

    override fun updateMoviesCatalogue() {
        // fetch all movies using default provider
        moviesPresenter.loadMovies(getString(R.string.filmworld).toLowerCase(Locale.getDefault()))
    }

    override fun showMovies(movies: List<Movie>) {
        hideLoading()

        // if no information was retrieved, there's nothing to update
        if (movies.isEmpty()) return

        moviesAdapter.updateMovies(movies)
    }

    override fun showError(error: String) {
        binding.apply {
            helperText.text = error
            helperText.visibility = View.VISIBLE
            helperButton.visibility = View.VISIBLE
        }
    }

    override fun showLoading() {
        binding.helperText.text = getString(R.string.loading)
        binding.helperText.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.helperText.visibility = View.GONE
        binding.helperButton.visibility = View.GONE
    }
}
