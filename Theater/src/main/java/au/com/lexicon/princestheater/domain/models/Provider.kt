package au.com.lexicon.princestheater.domain.models

data class Provider(
    val name: String,
    val prefix: String
)