package au.com.lexicon.princestheater.core.injection.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import org.koin.core.KoinComponent
import org.koin.core.scope.Scope

/**
 * Utility class taken from koin to ease lifecycle observations
 */
class ScopeObserver(val event: Lifecycle.Event, val target: Any, val scope: Scope) :
    LifecycleObserver, KoinComponent {

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        if (event == Lifecycle.Event.ON_STOP) {
            scope._koin._logger.debug("$target received ON_STOP")
            scope.close()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        if (event == Lifecycle.Event.ON_DESTROY) {
            scope._koin._logger.debug("$target received ON_DESTROY")
            scope.close()
        }
    }
}