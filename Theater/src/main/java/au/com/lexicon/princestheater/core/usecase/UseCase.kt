package au.com.lexicon.princestheater.core.usecase

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Base use case class based on the clean architecture and mixes
 * co-routines to it. The purpose of having Any? allows us to have
 * nullable returned from the use cases. This can help in many ways including
 * testing.
 *
 * A use case has only job or a purpose. It should not do multiple things in
 * single definition. When defining the use cases, make sure the use case does
 * singular defined operations, but they may take as much time as required.
 *
 * Also, cancellation is allowed if a use case takes too long. The calling
 * class should call the unsubscribe() method, which will essentially try to
 * cancel the coroutine, and any other jobs attached to it.
 */
abstract class UseCase<out T, in P> where T : Any? {

    // A job that will execute what we need.
    private var job: Job = Job()
    var backgroundContext: CoroutineContext = Dispatchers.IO
    var foregroundContext: CoroutineContext = Dispatchers.Main

    // Allows for injecting uncaught exception handlers into the use case scope
    var context: CoroutineContext = EmptyCoroutineContext

    /**
     * Primary function that will be executed by the concrete classes.
     * The method runs inside a coroutine container and can have other suspend
     * methods called inside it.
     *
     * @param params Could be any object or EmptyParams, if nothing needs to be sent to the use case
     *
     * @return UseCaseResult<Exception, T> T is the required output for success
     */
    protected abstract suspend fun executeOnBackground(params: P): UseCaseResult<Exception, T>

    /**
     * The execute method is called by the class which instantiated the use case.
     * The method then spawns a coroutine and defines a scope for it. Once the scope
     * reaches it ends; whether as an error or success, it will return the result
     * back to the caller.
     *
     * @param params Could be any object or EmptyParams, if nothing needs to be sent to the use case
     * @param onStart a lambda or a function that needs to invoked before the use case starts
     * @param onFinish a lambda or a function that needs to invoked after the use case finishes
     * @param onError a lambda or a function that needs to invoked when the result is an error
     * @param onCancel a lambda or a function that needs to invoked when the use case was cancelled
     * @param onSuccess a lambda or a function that needs to invoked when the result was retrieved
     *
     * Note that the method does not return anything and instead calls the methods listed to make
     * sure any waiting callers can get their results in order.
     */
    fun execute(
        params: P,
        onSuccess: (T) -> Unit = {},
        onError: (Exception) -> Unit = {},
        onCancel: (Exception) -> Unit = {},
        onStart: () -> Unit = {},
        onFinish: () -> Unit = {}
    ) {
        // unsubscribe, if a second execute is called before the first one is finished
        unsubscribe()
        // create a job using a context
        job = CoroutineScope(foregroundContext).launch(context) {
            // call the onStart callback before starting operation
            onStart()
            try {
                // Switch context before executing operation
                withContext(backgroundContext) {
                    executeOnBackground(params)
                }
            } catch (e: Exception) {
                // If the coroutine was cancelled for some reason, return a cancel result
                if (e is CancellationException) {
                    UseCaseResult.Cancel(
                        e
                    )
                } else {
                    // otherwise, return the error itself
                    UseCaseResult.Error(
                        e
                    )
                }
            }.map(
                // UseCaseResult class will fold the result based on status
                cancelled = { onCancel(it) },
                errored = { onError(it) },
                succeeded = { onSuccess(it) }
            )
        }
        // If a job was completed, it will call finish
        // if it was canceled, we don't call finish.
        // This is entirely a design choice, this could act like a finally
        // clause as well, which gets called regardless of completion status
        if (job.isCompleted) {
            onFinish()
        }
    }

    /**
     * Cancel the job when this method is called.
     * It's open for the caller and the execute method to
     * call when require.d
     */
    fun unsubscribe() = job.cancel()

    class EmptyParams
}
