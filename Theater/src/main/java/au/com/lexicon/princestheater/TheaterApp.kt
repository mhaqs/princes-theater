package au.com.lexicon.princestheater

import android.app.Application
import au.com.lexicon.princestheater.core.injection.theaterApp
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * The application class is an entry point for the Koin
 * framework initiation.
 *
 * - This could be a component as well. For example TheaterAppComponent, and then
 * this component can be loaded here using the `startKoin` dsl.
 */
class TheaterApp : Application() {

    override fun onCreate() {
        super.onCreate()

        // Starts the koin framework
        startKoin {
            // defines the debugging level
            androidLogger(Level.DEBUG)

            // creates a global context for the app modules to use
            androidContext(this@TheaterApp)

            //tells the koin framework that a configuration file exists
            androidFileProperties()

            // loads the given module and any modules attached to it
            modules(theaterApp)
        }
    }
}