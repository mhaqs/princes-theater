package au.com.lexicon.princestheater.domain.repository

import au.com.lexicon.princestheater.core.usecase.UseCaseResult
import au.com.lexicon.princestheater.data.api.SiteApi
import au.com.lexicon.princestheater.data.models.Movie
import au.com.lexicon.princestheater.domain.exceptions.ServerException
import au.com.lexicon.princestheater.domain.extensions.generateProviderIdByNakedId
import au.com.lexicon.princestheater.domain.extensions.movieIdIsNaked
import au.com.lexicon.princestheater.domain.extensions.nakedId
import au.com.lexicon.princestheater.domain.models.MovieDetail
import kotlinx.coroutines.CancellationException
import kotlin.IllegalArgumentException

/**
 * The Movie Repository is an interface on top of the REST api that allows
 * us to create custom operations using the base api. The repository is also
 * responsible for executing use cases and returning the results of the use
 * cases back as either a result or a failure using the #UseCaseResult wrapper.
 *
 * Please note that the repository is not a mirror of the REST API interface.
 * It is in fact very different wrt the domain we're interested in. The
 * API can provide us the information we need, but we want to consume it in a
 * custom fashion, and the repository allows us to do that.
 */
interface MovieRepository {
    suspend fun getMovies(provider: String): UseCaseResult<Exception, List<Movie>>
    suspend fun getMovieDetail(provider: String, movieId: String): UseCaseResult<Exception, MovieDetail>
    suspend fun getProviderPricingForMovie(providers: List<String>, nakedMovieId: String): UseCaseResult<Exception, MovieDetail>
}

class MovieRepositoryImpl(
    private val siteApi: SiteApi
) : MovieRepository {

    private val moviePriceDetail = mutableListOf<MovieDetail>()

    override suspend fun getMovies(provider: String): UseCaseResult<Exception, List<Movie>> {
        if (provider.isEmpty()) return UseCaseResult.Cancel(CancellationException())

        val moviesResponse = siteApi.fetchMovies(provider)

        // If the api returned an error, tell the caller what the error is
        return if (moviesResponse.movies.isEmpty()) UseCaseResult.Error(ServerException())

        // Otherwise, we have what we need
        else UseCaseResult.Success(moviesResponse.movies)
    }

    override suspend fun getMovieDetail(provider: String, movieId: String): UseCaseResult<Exception, MovieDetail> {
        if (provider.isEmpty() || movieId.isEmpty()) return UseCaseResult.Cancel(
            CancellationException())
        if (movieIdIsNaked(movieId)) return UseCaseResult.Error(IllegalArgumentException("Movie ID must be in the form of: ab1234, was $movieId"))

        val movie = siteApi.fetchMovie(provider, movieId)

        var cachedMovie = moviePriceDetail.find { it.ID == movie.nakedId }
        if (cachedMovie == null) {
            val movieDetail = MovieDetail(movie.nakedId)
            movieDetail.setMovieByProvider(provider, movie)
            cachedMovie = movieDetail
        } else {
            cachedMovie.setMovieByProvider(provider, movie)
        }

        moviePriceDetail.add(cachedMovie)
        return UseCaseResult.Success(cachedMovie)
    }

    override suspend fun getProviderPricingForMovie(providers: List<String>, nakedMovieId: String): UseCaseResult<Exception, MovieDetail> {
        if (providers.isEmpty()) return UseCaseResult.Cancel(CancellationException())
        if (!movieIdIsNaked(nakedMovieId)) return UseCaseResult.Error(IllegalArgumentException("Naked ID must be numeric only, was $nakedMovieId"))

        for (provider in providers) {
            getMovieDetail(provider, generateProviderIdByNakedId(provider, nakedMovieId).toString())
        }

        val cachedMovie = moviePriceDetail.find { it.ID == nakedMovieId }

        return if (cachedMovie== null) UseCaseResult.Error(ServerException())
        else UseCaseResult.Success(cachedMovie)
    }
}