import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(Versions.androidCompileSdk)
    buildToolsVersion(Versions.androidBuildTools)

    defaultConfig {
        minSdkVersion(Versions.androidMinSdk)
        targetSdkVersion(Versions.androidTargetSdk)
        applicationId = Versions.applicationId
        versionCode = 1
        versionName = "1.0.0"
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isDebuggable = false
            isShrinkResources = true
            isJniDebuggable = false
            isRenderscriptDebuggable = false
            isPseudoLocalesEnabled = false
            isZipAlignEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }

        getByName("debug") {
            isMinifyEnabled = false
            isDebuggable = true
            isTestCoverageEnabled = true
        }
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(AndroidDependencies.androidSupportAppCompatV7)
    implementation(AndroidDependencies.androidSupportDesign)
    implementation(AndroidDependencies.androidSupportRecyclerView)
    implementation(AndroidDependencies.androidSupportConstraintLayout)
    implementation(AndroidDependencies.androidSupportAnnotations)

    testImplementation(TestDependencies.kotlinJUnit)
    testImplementation(TestDependencies.kotlinCoroutineTest)
    testImplementation(TestDependencies.mockitoKotlin)
    testImplementation(TestDependencies.mockitoKotlin2)

    androidTestImplementation(TestDependencies.testJunit)
    androidTestImplementation(TestDependencies.testRules)
    androidTestImplementation(TestDependencies.testRunner)
    androidTestImplementation(TestDependencies.kotlinCoroutineTest)
    androidTestImplementation(TestDependencies.mockitoKotlin)
    androidTestImplementation(TestDependencies.mockitoKotlin2)
    androidTestUtil(TestDependencies.testOrchestrator)

    // Kotlin
    implementation(AndroidDependencies.kotlin8)

    implementation(AppDependencies.koinCore)
    implementation(AppDependencies.koinCoreExt)
    implementation(AppDependencies.koinAndroid)
    testImplementation(TestDependencies.koinTest)
    androidTestImplementation(TestDependencies.koinTest)

    implementation(AndroidDependencies.lifecycle)
    kapt(AndroidDependencies.lifecycleCompiler)
    testImplementation(TestDependencies.lifecycleTest)
    androidTestImplementation(TestDependencies.lifecycleTest)

    // Networking
    implementation(AppDependencies.retrofit)
    implementation(AppDependencies.retrofitConverterScalar)
    implementation(AppDependencies.retrofitConverterGson)
    implementation(AppDependencies.okHttp)
    implementation(AppDependencies.okHttpLogger)
    implementation(AndroidDependencies.gson)

    // Coroutines
    implementation(AndroidDependencies.kotlinCoroutineCore)
    implementation(AndroidDependencies.kotlinCoroutineAndroid)

    // Image
    implementation(AppDependencies.glide)
    kapt(AppDependencies.glideCompiler)

    // Time
    implementation(AppDependencies.jodaTime)
    implementation(AppDependencies.timber)
}

tasks.withType<KotlinCompile>().all {
    kotlinOptions.freeCompilerArgs += listOf(
        "-Xuse-experimental=kotlinx.serialization.ImplicitReflectionSerializer",
        "-Xuse-experimental=kotlinx.serialization.UnstableDefault",
        "-Xuse-experimental=kotlinx.coroutines.ExperimentalCoroutinesApi",
        "-Xuse-experimental=kotlinx.coroutines.ObsoleteCoroutinesApi"
    )
}