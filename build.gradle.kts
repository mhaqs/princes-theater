
buildscript {

    repositories {
        google()
        jcenter()
        maven { setUrl("https://maven.fabric.io/public") }
        maven { setUrl("https://jitpack.io") }
    }
    dependencies {
        classpath(AndroidDependencies.androidGradle)
        classpath(AndroidDependencies.kotlinGradle)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven { setUrl("https://maven.fabric.io/public") }
        maven { setUrl("https://jitpack.io") }
        // artifacts are published to this repository
        maven { setUrl("https://kotlin.bintray.com/kotlinx") }
    }
}

tasks {
    val clean by registering(Delete::class) {
        delete(buildDir)
    }
}