@Suppress("unused")
object Versions {

    // Android
    const val applicationId = "au.com.lexicon.princestheater"
    const val androidGradle = "4.0.0"
    const val androidBuildTools = "30.0.1"
    const val androidCompileSdk = 29
    const val androidMinSdk = 21
    const val androidTargetSdk = 29

    const val constraintLayout = "1.1.3"
    const val supportLibrary = "1.0.2"
    const val materialDesignLibrary = "1.1.0"
    const val multiDex = "2.0.0"

    // App Libraries
    const val koin = "2.1.5"
    const val glide = "4.8.0"
    const val gson = "2.8.6"
    const val kotlin = "1.3.72"
    const val kotlinCoroutine = "1.2.1"
    const val kotlinCoroutineTest = "1.2.1"
    const val okHttp = "4.4.1"
    const val retrofit = "2.8.0"
    const val timber = "4.6.1"
    const val lifecycle = "2.2.0"
    const val arch = "2.0.0-rc01"
    const val joda = "2.10.2"

    // Test Libraries
    const val assertJ = "3.8.0"
    const val jUnit = "4.12"
    const val mockitoKotlin = "1.5.0"
    const val mockitoKotlin2 = "2.1.0"
    const val test = "1.1.0"
}

@Suppress("unused")
object AndroidDependencies {
    const val androidGradle = "com.android.tools.build:gradle:${Versions.androidGradle}"
    const val androidSupportAnnotations = "androidx.annotation:annotation:${Versions.supportLibrary}"
    const val androidSupportAppCompatV7 = "androidx.appcompat:appcompat:${Versions.supportLibrary}"
    const val androidSupportConstraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val androidSupportDesign = "com.google.android.material:material:${Versions.materialDesignLibrary}"
    const val androidSupportRecyclerView = "androidx.recyclerview:recyclerview:${Versions.supportLibrary}"
    const val androidSupportMultidex = "androidx.multidex:multidex:${Versions.multiDex}"
    const val androidDatabindingCompiler = "androidx.databinding:databinding-compiler:${Versions.androidGradle}"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val kotlin8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val kotlinCoroutineCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinCoroutine}"
    const val kotlinCoroutineAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlinCoroutine}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycle}"
}

@Suppress("unused")
object AppDependencies {

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    const val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okHttp}"
    const val okHttpLogger = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    const val retrofitConverterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofitConverterScalar = "com.squareup.retrofit2:converter-scalars:${Versions.retrofit}"
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
    const val koinCore = "org.koin:koin-core:${Versions.koin}"
    const val koinCoreExt = "org.koin:koin-core-ext:${Versions.koin}"
    const val koinAndroid = "org.koin:koin-android:${Versions.koin}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin}"
    const val jodaTime = "joda-time:joda-time:${Versions.joda}"
}

@Suppress("unused")
object TestDependencies {

    const val testRules = "androidx.test:rules:${Versions.test}"
    const val testRunner = "androidx.test:runner:${Versions.test}"
    const val testJunit = "androidx.test.ext:junit:${Versions.test}"
    const val lifecycleTest = "androidx.arch.core:core-testing:${Versions.arch}"
    const val assertj = "org.assertj:assertj-core:${Versions.assertJ}"
    const val junit = "junit:junit:${Versions.jUnit}"
    const val kotlinJUnit = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.kotlin}"
    const val kotlinCoroutineTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.kotlinCoroutineTest}"
    const val mockitoKotlin = "com.nhaarman:mockito-kotlin:${Versions.mockitoKotlin}"
    const val mockitoKotlin2 = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlin2}"
    const val koinTest = "org.koin:koin-test:${Versions.koin}"
    const val testOrchestrator = "androidx.test:orchestrator:${Versions.test}"
    const val multidexInstrument = "androidx.multidex:multidex-instrumentation:${Versions.multiDex}"
}
